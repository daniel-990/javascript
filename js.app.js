/*
    * para importar todo desde un modulo 
    import * as module from './js_components/app.component.js'; 
*/

/*
    * para importar solo una funcion determinada desde un modulo 

    import {init} from './js_components/app.menu.js'; 
*/

/*
    *llamamos las funciones alojadas en './js_components/app.component.js' 
*/

// module.default();
// module.doStuff();
// module.init();  

/*
    para llamar una sola funcion en nuestro js.app.js
*/
//let val = init(); ó init();

import {menu} from './js_components/app.menu.js'; 
import * as moduleBody from './js_components/app.body.js';
import {footer} from './js_components/app.footer.js'; 

menu();
moduleBody.default(); 
footer();