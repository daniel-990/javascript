import {moduleMenu} from '../bd_json/menu.js';
import {dbRemota} from '../bd_json/db.javascript.js'; 

const data = moduleMenu; /** data local */
const data_ = dbRemota.db;

export default () => {
    fetch(data_)
    .then((resp) => resp.json()) 
    .then(function(datos) {
       
        datos.results.map( bodyP =>{
            //console.log(bodyP);
            const contenedorbody = document.querySelector("#html_body");
            let htmlB = `
                <hr>
                <div class="container">
                    <figure class="figure">
                        <img src="${bodyP.picture.medium}" class="figure-img img-fluid rounded" alt="...">
                        <figcaption class="figure-caption">
                            <h4>
                                ${bodyP.email}
                                <br> 
                                <span class="badge badge-warning">Edad: ${bodyP.dob.age} años</span>
                            </h4>
                            <p>
                               Ciudad: ${bodyP.location.city}
                               Value: ${bodyP.id.value}
                               Cel: ${bodyP.cell}
                            </p>
                        </figcaption>
                    </figure>
                </div>
            `;
            contenedorbody.innerHTML += htmlB;

        })
    })
};